import requests
from urllib.parse import quote
import numpy as np
from minisom import MiniSom
import matplotlib.pyplot as plt
import sys
import pickle
import os

# Sanity configuration
project_id = 'ulrlyhdt'
dataset = 'production'
token = 'skyWFz4h38zFHzdmRrn20eNWcpB7w524XAjskRCmP9C8EoeQvwLNqNv3XwKh2L0mL0OPstbnSlJKMGwZumq1biyo9ebJ2VLXyzVhxa0btfnWAVxJM76jd7AAYx1vCObAhbt6Qt8YRV19H2H1qpJvBBVWdh5t0QqKf2EFT21aQRe76VceLPOP'  # Replace with your Sanity API token

# Sanity API URLs
query_url = f'https://{project_id}.api.sanity.io/v2021-06-07/data/query/{dataset}'
mutation_url = f'https://{project_id}.api.sanity.io/v2022-03-07/data/mutate/{dataset}'

# Headers with API token
headers = {
    'Authorization': f'Bearer {token}',
    'Content-Type': 'application/json'
}

documents_file = "documents.pkl"
som_file = "som.pkl"

def fetch_documents():
    #Fetches documents from Sanity that contain vectors and names.
    query = '*[defined(vector)]{_id, vector, name}'#
    encoded_query = quote(query)
    response = requests.get(f'{query_url}?query={encoded_query}', headers=headers)
    
    if response.status_code == 200:
        documents = response.json().get('result', [])
        print('request matched')
        with open(documents_file, 'wb') as f:
            pickle.dump(documents, f)
        return documents
    else:
        print(f"Error fetching documents: {response.status_code}")
        print(response.text)
        return []

def train_som(documents):
    """Trains a SOM with the fetched documents and visualizes the process."""
    if not documents:
        print("No documents found.")
        return None

    vectors = np.array([doc['vector'] for doc in documents])
    titles = [doc['name'] for doc in documents]

    # Initialize the SOM
    som = MiniSom(13, 21, len(vectors[0]), sigma=5, learning_rate=0.1)
    som.pca_weights_init(vectors)

    # Custom visualization loop
    plt.figure(figsize=(7, 7))
    for i in range(0, 1000, 50):  # Visualize every 50 iterations
        som.train_batch(vectors, 50)
        activation_map = som.activation_response(vectors)
        plt.imshow(activation_map, cmap='Blues')
        plt.title(f'Activation Map after {i} iterations')
        plt.colorbar()
        plt.pause(0.5)
        if i < 950:
            plt.clf()

    plt.show()
    
    with open(som_file, 'wb') as f:
        pickle.dump(som, f)
    return som

def update_documents_with_som(documents, som):
    """Updates documents in Sanity with the SOM's embedding."""
    if not documents or som is None:
        print("No SOM or documents available for update.")
        return
    
    for doc in documents:
        vector = np.array(doc['vector'])
        winner_position = som.winner(vector)

        node = [int(coord) for coord in winner_position]
        
        # Prepare the mutation request to update the document
        mutation = {
            "mutations": [
                {
                    "patch": {
                        "id": doc['_id'],
                        "set": {
                            "node": node
                        }
                    }
                }
            ]
        }

        # Send the mutation request to Sanity
        response = requests.post(mutation_url, json=mutation, headers=headers)
        if response.status_code == 200:
            print(f"Successfully updated document {doc['_id']} with SOM embedding.")
        else:
            print(f"Error updating document {doc['_id']}: {response.status_code}")
            print(response.text)

def load_documents():
    """Loads documents from file if available, otherwise fetches them."""
    if os.path.exists(documents_file):
        print("Loading documents from file.")
        with open(documents_file, 'rb') as f:
            return pickle.load(f)
    else:
        print("Documents file not found, fetching from Sanity.")
        return fetch_documents()

def load_som():
    """Loads SOM from file if available, otherwise trains a new one."""
    if os.path.exists(som_file):
        print("Loading SOM from file.")
        with open(som_file, 'rb') as f:
            return pickle.load(f)
    else:
        print("SOM file not found, training a new SOM.")
        documents = load_documents()
        return train_som(documents)

if __name__ == "__main__":
    command = sys.argv[1] if len(sys.argv) > 1 else None
    
    if command == "fetch":
        documents = fetch_documents()       # Step 1: Fetch documents from Sanity
    elif command == "train":
        documents = load_documents()       # Fetch the documents first if needed
        som = train_som(documents)          # Step 2: Train and visualize SOM
    elif command == "update":
        documents = load_documents()       # Fetch the documents first if needed
        som = load_som()          # Make sure SOM is trained before updating
        update_documents_with_som(documents, som)  # Step 3: Update documents with SOM embeddings
    else:
        print("Please specify a command: 'fetch', 'train', or 'update'.")