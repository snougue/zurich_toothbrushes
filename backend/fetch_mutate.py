import requests
from sentence_transformers import SentenceTransformer
from urllib.parse import quote

# Sanity API configuration
project_id = 'ulrlyhdt'  # Replace with your actual project ID
dataset = 'production'   # Replace with your dataset name
token = 'skyWFz4h38zFHzdmRrn20eNWcpB7w524XAjskRCmP9C8EoeQvwLNqNv3XwKh2L0mL0OPstbnSlJKMGwZumq1biyo9ebJ2VLXyzVhxa0btfnWAVxJM76jd7AAYx1vCObAhbt6Qt8YRV19H2H1qpJvBBVWdh5t0QqKf2EFT21aQRe76VceLPOP'  # Replace with your Sanity API token

# Sanity API URLs
query_url = f'https://{project_id}.api.sanity.io/v2022-03-07/data/query/{dataset}'
mutation_url = f'https://{project_id}.api.sanity.io/v2022-03-07/data/mutate/{dataset}'

# Headers with API token
headers = {
    'Authorization': f'Bearer {token}',
    'Content-Type': 'application/json'
}

def fetch_documents():
    # Define the GROQ query and encode it properly
    query = '*[_type == "video" && !defined(vector)]{_id, description}'
    encoded_query = quote(query)

    # Make the GET request to Sanity's API
    response = requests.get(f'{query_url}?query={encoded_query}', headers=headers)
    
    # Check if the request was successful
    if response.status_code == 200:
        documents = response.json().get('result', [])
        return documents
    else:
        print(f"Error fetching documents: {response.status_code}")
        print(response.text)
        return []

def generate_embeddings(texts):
    # Load a pre-trained SBERT model
    model = SentenceTransformer('paraphrase-MiniLM-L6-v2')
    
    # Generate embeddings
    embeddings = model.encode(texts)
    
    return embeddings

def update_document_with_embedding(doc_id, embedding):
    # Prepare the mutation request to add the embedding vector to the document
    mutation = {
        "mutations": [
            {
                "patch": {
                    "id": doc_id,
                    "set": {
                        "vector": embedding.tolist()  # Convert numpy array to list
                    }
                }
            }
        ]
    }

    # Send the mutation request to Sanity
    response = requests.post(mutation_url, json=mutation, headers=headers)

    # Check if the update was successful
    if response.status_code == 200:
        print(f"Successfully updated document {doc_id} with embedding.")
    else:
        print(f"Error updating document {doc_id}: {response.status_code}")
        print(response.text)

def process_and_update_documents():
    # Fetch documents from Sanity
    documents = fetch_documents()

    if not documents:
        print("No documents found.")
        return

    # Extract the texts to process
    texts = [doc.get('description', '') for doc in documents]

    # Generate embeddings for the texts
    embeddings = generate_embeddings(texts)

    # Update each document in Sanity with the corresponding embedding
    for doc, embedding in zip(documents, embeddings):
        update_document_with_embedding(doc['_id'], embedding)

if __name__ == "__main__":
    process_and_update_documents()