import requests
from urllib.parse import quote

# Sanity configuration
project_id = 'ulrlyhdt'
dataset = 'production'
token = 'skyWFz4h38zFHzdmRrn20eNWcpB7w524XAjskRCmP9C8EoeQvwLNqNv3XwKh2L0mL0OPstbnSlJKMGwZumq1biyo9ebJ2VLXyzVhxa0btfnWAVxJM76jd7AAYx1vCObAhbt6Qt8YRV19H2H1qpJvBBVWdh5t0QqKf2EFT21aQRe76VceLPOP'  # Replace with your Sanity API token

# Sanity API URLs
query_url = f'https://{project_id}.api.sanity.io/v2021-06-07/data/query/{dataset}'
mutation_url = f'https://{project_id}.api.sanity.io/v2022-03-07/data/mutate/{dataset}'

# Headers with API token
headers = {
    'Authorization': f'Bearer {token}',
    'Content-Type': 'application/json'
}

def fetch_documents():
    """Fetches documents from Sanity that contain vectors and names."""
    query = '*[]{}'
    encoded_query = quote(query)
    response = requests.get(f'{query_url}?query={encoded_query}', headers=headers)
    
    if response.status_code == 200:
        documents = response.json().get('result', [])
        print('request matched')
        return documents
    else:
        print(f"Error fetching documents: {response.status_code}")
        print(response.text)
        return []

def update_documents(documents):
    """Updates documents in Sanity with the SOM's embedding."""
    if documents is None:
        print("No documents available for update.")
        return
    
    for doc in documents:
        # Prepare the mutation request to update the document
        mutation = {
            "mutations": [
                {
                    "patch": {
                        "id": doc['_id'],
                        "set": {
                            "vector": None  # Assuming 'narrative' is a field you want to update
                        }
                    }
                }
            ]
        }

        # Send the mutation request to Sanity
        response = requests.post(mutation_url, json=mutation, headers=headers)
        if response.status_code == 200:
            print(f"Successfully updated document {doc['_id']}.")
        else:
            print(f"Error updating document {doc['_id']}: {response.status_code}")
            print(response.text)

if __name__ == "__main__":
    documents = fetch_documents()  # Step 1: Fetch documents from Sanity
    update_documents(documents)    # Step 2: Update documents in Sanity