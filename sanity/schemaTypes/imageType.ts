import {defineField, defineType} from 'sanity'

export const imageType = defineType({
  name: 'image_component',
  title: 'image_component',
  type: 'document',
  fields: [
    defineField({
      name: 'name',
      type: 'string',
    }),
    defineField({
      name: 'path',
      type: 'string',
    }),
    defineField({
      name: 'coordinates',
      type: 'geopoint',
    }),
    defineField({
      name: 'tag',
      type: 'string',
    }),
    defineField({
      name: 'description',
      type: 'text',
    }),
    defineField({
      name: 'url',
      type: 'string',
    }),
    defineField({
      name: 'narrative',
      type: 'number',
    }),
    defineField({
      name: 'node',
      type: 'array',
      of: [{type: 'number'}],
      readOnly: true,
    }),
    defineField({
      name: 'vector',
      type: 'array',
      of: [{type: 'number'}],
      readOnly: true,
    }),
  ],
})
