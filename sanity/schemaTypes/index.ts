import {eventType} from "./eventType"
import { imageType } from "./imageType"
import { textType } from "./textType"
import { videoType } from "./videoType"

export const schemaTypes = [
    eventType,
    videoType,
    textType,
    imageType
]
