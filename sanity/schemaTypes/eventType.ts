import {defineField, defineType} from 'sanity'

export const eventType = defineType({
  name: 'event',
  title: 'Event',
  type: 'document',
  fields: [
    defineField({
      name: 'name',
      type: 'string',
    }),
    defineField({
        name: 'ytbURL',
        type: 'string',
    }),
    defineField({
        name: 'coordinates',
        type: 'geopoint',
    }),
    defineField({
        name: 'tag',
        type: 'string',
    }),
  ],
})
