import {defineCliConfig} from 'sanity/cli'

export default defineCliConfig({
  api: {
    projectId: 'ulrlyhdt',
    dataset: 'production'
  }
})
