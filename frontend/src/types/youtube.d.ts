declare namespace YT {
    interface Player {
      playVideo: () => void;
      cueVideoById: (videoId: string) => void;
      loadVideoById: (videoId: string) => void;
      getPlayerState: () => number;
    }
  
    interface PlayerState {
      ENDED: number;
      PLAYING: number;
      PAUSED: number;
      BUFFERING: number;
      CUED: number;
    }
  
    var PlayerState: PlayerState;
  }
  
  declare global {
    interface Window {
      YT: typeof YT;
      onYouTubeIframeAPIReady: () => void;
    }
  }
  
  export {};