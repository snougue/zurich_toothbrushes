// useCameraAnimation.tsx

import { useState, useRef } from "react";
import { animate, MotionValue, AnimationPlaybackControls } from "framer-motion";

export const useCameraAnimation = (
  cameraX: MotionValue<number>,
  cameraY: MotionValue<number>,
  cameraZ: MotionValue<number>,
  points: [number, number, number][]
) => {
  const [isAnimating, setIsAnimating] = useState<boolean>(false);
  const [paused, setPaused] = useState<boolean>(false);
  const controlsRef = useRef<{ x: AnimationPlaybackControls | null, y: AnimationPlaybackControls | null, z: AnimationPlaybackControls | null }>({
    x: null,
    y: null,
    z: null,
  });

  const delay = (ms: number) => new Promise(resolve => setTimeout(resolve, ms));

  const animateToNextPoint = async (point: [number, number, number]) => {
    controlsRef.current.x = animate(cameraX, point[0], { duration: 1, ease: "easeInOut" });
    controlsRef.current.y = animate(cameraY, point[1], { duration: 1, ease: "easeInOut" });
    controlsRef.current.z = animate(cameraZ, point[2], { duration: 1, ease: "easeInOut" });

    // Await the completion of all animations
    await Promise.all([
      controlsRef.current.x,
      controlsRef.current.y,
      controlsRef.current.z,
    ]);
  };

  const startAnimation = async () => {
    setIsAnimating(true);
    setPaused(false);

    for (const point of points) {
      // Check if the animation is paused
      while (paused) {
        await delay(100);  // Polling delay while paused
      }
      await animateToNextPoint(point);
      await delay(5000);
    }

    setIsAnimating(false);
  };

  const pauseAnimation = () => {
    setPaused(true);
    controlsRef.current.x?.stop();
    controlsRef.current.y?.stop();
    controlsRef.current.z?.stop();
  };

  const resumeAnimation = () => {
    setPaused(false);
  };

  return { isAnimating, startAnimation, pauseAnimation, resumeAnimation };
};

export function shuffleArray<T>(array: T[]): T[] {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

export function useCameraPosition(
    cameraX: MotionValue<number>,
    cameraY: MotionValue<number>,
    cameraZ: MotionValue<number>,
    point: [number, number, number]) { 
    
    animate(cameraX, point[0], { duration: 2, ease: "easeInOut" });
    animate(cameraY, point[1], { duration: 2, ease: "easeInOut" });
    animate(cameraZ, point[2], { duration: 2, ease: "easeInOut" });
    };