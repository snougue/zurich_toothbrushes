import React, { useRef } from 'react';
import { PerspectiveCamera } from '@react-three/drei';
import { useFrame } from '@react-three/fiber';
import * as THREE from '../../node_modules/@types/three';
import { Vector3 } from '../../node_modules/@types/three';


type CameraPathProps = {
  path: [number, number, number][];
  animate: boolean;
};

const CameraPath: React.FC<CameraPathProps> = ({ path, animate }) => {
  const cameraRef = useRef<THREE.PerspectiveCamera>(null);
  const pathIndex = useRef(0);
  const speed = 0.01;

  useFrame(() => {
    if (animate && cameraRef.current && path.length > 0) {
      const currentPoint = new Vector3(...path[pathIndex.current]);
      const nextPoint = new Vector3(...path[(pathIndex.current + 1) % path.length]);

      cameraRef.current.position.lerpVectors(currentPoint, nextPoint, speed);
      console.log(cameraRef.current.position)
      if (cameraRef.current.position.distanceTo(nextPoint) < 0.1) {
        pathIndex.current = (pathIndex.current + 1) % path.length;
      }
    }
  });

  return (
    <PerspectiveCamera
      ref={cameraRef}
      makeDefault
      position={[0, 0, 5]}
    />
  );
};

export default CameraPath;