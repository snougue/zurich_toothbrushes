import { MapProvider, Map } from "react-map-gl";
import "mapbox-gl/dist/mapbox-gl.css";


interface MapComponentProps {
    lng?: number;
    lat?: number;
    zoom?: number;
    setMapInstance: React.Dispatch<React.SetStateAction<any>>;
  }
  
  const MapComponent: React.FC<MapComponentProps> = ({
    lng =  8.535210339278287,
    lat = 47.379510299643876,
    zoom = 15,
    setMapInstance,
  }) => {
    const handleMapLoad = (e: any) => {
      console.log("Map loaded:", e.target);
      setMapInstance(e.target); // Save the map instance in state
    };
  
    return (
      <Map
        id="myMap"
        mapboxAccessToken="pk.eyJ1IjoibGVvbi1oYW0iLCJhIjoiY2x6dmhkaTQ0MDcyZDJxc2R6dWx2eGJsYSJ9.yIlmFp0nhGD7aygFZQzFHg"
        initialViewState={{
          longitude: lng,
          latitude: lat,
          zoom: zoom,
        }}
        style={{ width: 8000, height: 8000}}
        mapStyle="mapbox://styles/leon-ham/cm07or7x200kf01ph5bkxbr2h"
        onLoad={handleMapLoad}
        interactive={false}
        
      />
    );
  };

  export default MapComponent