'use client';
import React, { useEffect, useRef } from 'react';
import { useFrame } from '@react-three/fiber';
import { PerspectiveCamera } from '@react-three/drei';
import { MotionValue } from 'framer-motion';
import * as THREE from '../../node_modules/@types/three';

interface CameraWithMotionProps {
  cameraX: MotionValue<number>;
  cameraY: MotionValue<number>;
  cameraZ: MotionValue<number>;
  isAnimating: boolean;
}


const CameraWithMotion:React.FC<CameraWithMotionProps> =({ cameraX, cameraY, cameraZ, isAnimating }) => {
  const cameraRef = useRef<THREE.PerspectiveCamera | null>(null);
  const initialRotation = useRef<THREE.Euler | null>(null);

  useFrame((state) => {
    const camera = state.camera;

    if (!cameraRef.current) {
      cameraRef.current = camera as THREE.PerspectiveCamera;
      initialRotation.current = camera.rotation.clone();
    }

    if (isAnimating) {
      camera.position.set(cameraX.get(), cameraY.get(), cameraZ.get());
    } else {
      cameraX.set(camera.position.x);
      cameraY.set(camera.position.y);
      cameraZ.set(camera.position.z);
    }

    if (initialRotation.current) {
      camera.rotation.copy(initialRotation.current);
    }

    
  });
  

  useEffect(() => {
    if (!cameraRef.current) return;

    const intervalId = setInterval(() => {
      const { x, y, z } = cameraRef.current!.rotation;
      console.log(`Rotation: X=${x.toFixed(3)}, Y=${y.toFixed(3)}, Z=${z.toFixed(3)}`);
    }, 1000);

    return () => clearInterval(intervalId);
  }, []);

  return (
    <PerspectiveCamera
      makeDefault
      fov={75}
      near={0.1}
      far={1000}
      position={[cameraX.get(), cameraY.get(), cameraZ.get()]}
    />
  );
}

export default CameraWithMotion