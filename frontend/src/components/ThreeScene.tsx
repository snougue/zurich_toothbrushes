"use client";
import React, { useState, useRef } from "react";
import { MapProvider } from "react-map-gl";
import { Canvas } from "@react-three/fiber";
import { Html, OrbitControls } from "@react-three/drei";
import { MotionValue, useMotionValue } from "framer-motion";
import { SanityDocument } from "next-sanity";

import CameraWithMotion from "./cameraWithMotion";
import { usePositionCalculations } from "./positionUtils";
import MapComponent from "./Map";
import EventVideo from "./eventVideo";
import { useCameraPosition, useCameraAnimation, shuffleArray} from "./useCameraAnimation";
import BackgroundGrid from "./backgroundGrid";
import TextElement from "./textElement";

interface MyComponentProps {
  events: SanityDocument[];
}

type MapboxInstance = {
  project: (lngLat: [number, number]) => { x: number; y: number };
};

const MyComponent: React.FC<MyComponentProps> = ({ events }) => {
  const [mapInstance, setMapInstance] = useState<MapboxInstance | null>(null);
  const htmlRef = useRef<HTMLDivElement>(null);
  const [isGrid, setIsGrid] = useState(false);
  
  const { gridPosById, mapPositionsById } = usePositionCalculations(mapInstance, events);

  const computePosition = (id: string): [number, number, number] => (
    isGrid ? gridPosById[id] : mapPositionsById[id]
  );

  const performActionBasedOnState = (prevState: boolean) => {
    
    if (prevState) {
      useCameraPosition(cameraX, cameraY, cameraZ, [0,0,700]);
      // Perform the action when switching from Grid to List view
    } else {
      useCameraPosition(cameraX, cameraY, cameraZ, [0,0,200]);
      // Perform the action when switching from List to Grid view
    }
  };

  const handleToggleGrid = () => {
    setIsGrid((prevState) => {
      performActionBasedOnState(prevState); // Use the previous state before toggling
      return !prevState; // Toggle the state
    });
  };

  // Initialize MotionValues for camera position
  const cameraX = useMotionValue(0);
  const cameraY = useMotionValue(0);
  const cameraZ = useMotionValue(700);

  // Use the camera animation hook
  const points: [number, number, number][] = Object.values(gridPosById).map(([x, y, z]) => [x, y, z + 80]);
  const shuffledPoints = shuffleArray(points);


  //Use the camera animation hook with the points array
  const { isAnimating, startAnimation, pauseAnimation, resumeAnimation } = useCameraAnimation(cameraX, cameraY, cameraZ, shuffledPoints);

  const [animationState, setAnimationState] = useState<'idle' | 'started' | 'paused'>('idle');

  const handleAnimationControl = () => {
    if (animationState === 'idle') {
      startAnimation();
      setAnimationState('started');
    } else if (animationState === 'started') {
      pauseAnimation();
      setAnimationState('paused');
    } else if (animationState === 'paused') {
      resumeAnimation();
      setAnimationState('started');
    }
  };



  return (
    <>
      <button
        onClick={handleToggleGrid}
        style={{
          position: "absolute",
          top: 20,
          left: 20,
          padding: "10px 20px",
          zIndex: 10000,
        }}
      >
      {isGrid ? 'Switch to Map Layout' : 'Switch to Grid Layout'}      
      </button>
      {isGrid && (
        <button
        onClick={handleAnimationControl}
        style={{
          position: "absolute",
          top: "20px",
          left: "200px",
          padding: "10px",
          zIndex: 10000,
        }}
      >
        {animationState === 'idle' ? 'Start Camera Path Animation' : 
         animationState === 'started' ? 'Pause Animation' : 'Resume Animation'}
      </button>
      )}
      
        <div
        style={{
          position: "absolute",
          top: "50%",
          left: "-140px", // Adjusted to move the text a bit to the right
          transform: "rotate(-90deg)", // Adjusted the transform to also center horizontally
          transformOrigin: "center",
          fontSize: "16px",
          zIndex: 10000,
          whiteSpace: "nowrap",
        }}
      >
        Where to Find the Best Toothbrush in Zurich ?
        </div>
    


      <MapProvider>
        <Canvas>
          <pointLight position={[10, 10, 10]} />

          {mapInstance && events.map((event) => (
      event._type === "video" ? (
        <EventVideo
          key={event._id}
          ref={htmlRef}
          event={event}
          position={computePosition(event._id)}
        />
      ) : (
        <TextElement
          key={event._id}
          content={event.content} // Assuming your text content is in this field
          position={computePosition(event._id)}
        />
      )
    ))}

          {!isGrid &&(<Html
            position={[0, 0, 0]}
            center
            transform
            distanceFactor={400}
            zIndexRange={[0, 10]}
          >
            <MapComponent setMapInstance={setMapInstance} />
          </Html>)}

          {isGrid && (
          <BackgroundGrid/>)}

          <OrbitControls
            enablePan={!isAnimating}
            enableZoom={!isAnimating}
            enableRotate={false}
            mouseButtons={{
              MIDDLE: 1,
              RIGHT: 2,
            }}
          />
          <CameraWithMotion
            cameraX={cameraX}
            cameraY={cameraY}
            cameraZ={cameraZ}
            isAnimating={isAnimating}
          />
        </Canvas>
      </MapProvider>
    </>
  );
};

export default MyComponent;