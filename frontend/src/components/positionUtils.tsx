// positionUtils.tsx
import { useMemo } from "react";
import { SanityDocument } from "next-sanity";

export function mapboxTo3D(mapInstance: any, lngLat: [number, number]): [number, number, number] {
  if (!mapInstance) {
    throw new Error("mapInstance is required");
  }

  const point = mapInstance.project(lngLat);

  const x = point.x - 4000;
  const y = -point.y + 4000;
  const z = 10;

  return [x, y, z];
}

export function calculateGridPositionsById(events: SanityDocument[], spacing: number = 125): Record<string, [number, number, number]> {
  const positions: Record<string, [number, number, number]> = {};

  events.forEach((event) => {
    // Assume each event has a "node" property with a two-dimensional vector [row, col]
    const [row, col] = event.node || [0, 0]; // Default to [0, 0] if no node is provided

    positions[event._id] = [
      (col - 10) * spacing,
      (row - 6) * spacing ,
      0,
    ];
    console.log(`Event ID: ${event._id}, Position: ${positions[event._id]}`);

  });

  return positions;
}

// Define the coordinate ranges for each tag
const tagFieldRanges: Record<string, { x: [number, number], y: [number, number], z: [number, number] }> = {
  "oliver": { x: [3000, -4000], y: [2250, 4000], z: [0, 0] },
  "arosa": { x: [3000, 4000], y: [0, -4000], z: [0, 0] },
  "autoethnography": { x: [2250, -3000], y: [-2250, -2000], z: [0, 0] },
  "bubble": { x: [-2250, -2000], y: [-1500, 3000], z: [0, 0] },
  "downtorome": { x: [-3000, -4000], y: [-2250, 4000], z: [0, 0] },
  "filmmaking": { x: [3000, -4000], y: [-3000, -3000], z: [0, 0] },
  "quantumcity": { x: [2250, 2000], y: [-2250, 3000], z: [0, 0] },
  "situationnisme": { x: [2250, -3000], y: [1500, 3000], z: [0, 0] },


  // Add more tags and their corresponding ranges here
};

function getRandomInRange(min: number, max: number): number {
  return Math.random() * (max - min) + min;
}

function calculatePositionByTag(tag: string): [number, number, number] {
  const range = tagFieldRanges[tag];
  if (!range) {
    // If the tag is not recognized, return a default position
    return [0, 0, 0];
  }

  const x = getRandomInRange(range.x[0], range.x[1]);
  const y = getRandomInRange(range.y[0], range.y[1]);
  const z = getRandomInRange(range.z[0], range.z[1]);

  return [x, y, z];
}

// Utility function to memoize and calculate positions
export function usePositionCalculations(mapInstance: any, events: SanityDocument[]) {
  const gridPosById = useMemo(() => calculateGridPositionsById(events), [events]);

  const mapPositionsById = useMemo(() => {
    if (!mapInstance) return {};
    return events.reduce((acc, event) => {
      if (event.coordinates) {
        // Use mapboxTo3D to calculate the position based on coordinates
        acc[event._id] = mapboxTo3D(mapInstance, event.coordinates);
      } else if (event.tag) {
        // If coordinates are missing, use the tag field for an alternative calculation
        acc[event._id] = calculatePositionByTag(event.tag);
      } else {
        // Fallback position if neither coordinates nor tag are available
        acc[event._id] = [0, 0, 0];
      }
      return acc;
    }, {} as Record<string, [number, number, number]>);
  }, [mapInstance, events]);

  return { gridPosById, mapPositionsById };
}