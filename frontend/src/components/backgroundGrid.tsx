'use client';
import React from 'react';
import { Html } from '@react-three/drei';
import styles from "@/app/styles/grid.module.css"

const Grid = ({rows, cols}:{rows: number; cols: number}) => {
    const items = [];
  
    for (let i = 0; i < rows * cols; i++) {
      items.push(
        <div key={i} className={styles['grid-item']}>
            ({i + 1})
        </div>);
    }
  
    return (
      <div className={styles['grid-container']} style={{ gridTemplateColumns: `repeat(${cols}, 1fr)` }}>
        {items}
      </div>
    );};

const BackgroundGrid =()=>{

    return (
            <Html transform distanceFactor={400} position={[0,0,-5]} zIndexRange={[0,10]}>
                <Grid rows={13} cols={21} />
            </Html>
    )
};

export default BackgroundGrid