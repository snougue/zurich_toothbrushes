import React, { useRef, forwardRef } from 'react';
import { DragControls, Html, Sphere } from '@react-three/drei';
import { motion as motion3D} from 'framer-motion-3d';
import { motion } from 'framer-motion' // Import from framer-motion-3d
import { SanityDocument } from 'next-sanity';
import * as THREE from '../../node_modules/@types/three';

interface EventVideoProps {
  content: string;
  position: [number, number, number];
}

const EventVideo = forwardRef<HTMLDivElement, EventVideoProps>(({ content, position }, ref) => {
  if (!content) return null;

  const groupRef = useRef<THREE.Group>(null);

  return (
    <>
    <motion3D.group
      initial={{ x: position[0], y: position[1], z: position[2] }}
      animate={{ x: position[0], y: position[1], z: position[2] }}
      transition={{ duration: 1, ease: "easeInOut" }}
      
    >

      <Html
        transform
        distanceFactor={400}
        renderOrder={2}
        zIndexRange={[10, 100]}
      >
        <motion.div
          animate={{ opacity: [0, 1], scale: [1] }}
          transition={{ duration: 0.5, ease: 'easeInOut' }}
          style={{ display: 'inline-block', overflow: 'hidden', fontSize: '6pt', height: '120px'}}
          whileHover={{
            scale: 1.2,
            transition: { duration: 0.3},
            zIndex: 1000
          }}
          className='videocomponent'
        >
          {content}
        </motion.div>
      </Html>
    </motion3D.group>
   
  </>
  );
});

export default EventVideo;