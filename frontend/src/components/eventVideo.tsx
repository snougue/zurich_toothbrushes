import React, { useRef, forwardRef } from "react";
import { DragControls, Html, Sphere } from "@react-three/drei";
import { motion as motion3D } from "framer-motion-3d";
import { motion } from "framer-motion"; // Import from framer-motion-3d
import { SanityDocument } from "next-sanity";
import * as THREE from "../../node_modules/@types/three";

interface TextElementProps {
  event: SanityDocument;
  position: [number, number, number];
}

const TextElement = forwardRef<HTMLDivElement, TextElementProps>(
  ({ event, position }, ref) => {
    if (!event.path) return null;

    const groupRef = useRef<THREE.Group>(null);
    const videoRef = useRef<HTMLVideoElement>(null);

    const handleMouseEnter = () => {
      if (videoRef.current) {
        // Set the video to a random position
        const randomTime = Math.random() * videoRef.current.duration;
        videoRef.current.currentTime = randomTime;
  
        // Play the video
        videoRef.current.play();
      }
    };
  
    const handleMouseLeave = () => {
      if (videoRef.current) {
        videoRef.current.pause();
      }
    };

    return (
      <>
        <motion3D.group
          initial={{ x: position[0], y: position[1], z: position[2] }}
          animate={{ x: position[0], y: position[1], z: position[2] }}
          transition={{ duration: 1, ease: "easeInOut" }}
        >
          <Html
            transform
            distanceFactor={400}
            renderOrder={2}
            zIndexRange={[10, 100]}
          >
            <motion.div
              animate={{ opacity: [0, 1], scale: [1] }}
              transition={{ duration: 0.5, ease: "easeInOut" }}
              style={{ display: "inline-block", overflow: "hidden" }}
              whileHover={{
                //scale: 1.2,
                //transition: { duration: 0.3 },
                //zIndex: 1000,
              }}
              className="videocomponent"
              onMouseEnter={handleMouseEnter}
              onMouseLeave={handleMouseLeave}
            >
              <video ref={videoRef} width={120} height={80}>
                <source src={`/videos/${event.path}`} type="video/mp4" />
              </video>
              {event.path}
            </motion.div>
          </Html>
        </motion3D.group>
      </>
    );
  }
);

export default TextElement;
