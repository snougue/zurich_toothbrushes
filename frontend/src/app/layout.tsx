import './globals.css';

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html lang="en">
      <head>
        <meta charSet="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Where to find the best toothbrush in Zurich</title>
        <link rel="icon" href="/favicon.ico" type="image/x-icon" sizes="32x32"></link>
      </head>
      <body>{children}</body>
    </html>
  );
}