import { SanityDocument } from "next-sanity";
import { sanityFetch } from "@/sanity/client";
import React, { Suspense } from "react";
const MyComponent = React.lazy(() => import("@/components/ThreeScene"));

const EVENTS_QUERY = `*[
  (_type == "video" && defined(node)) || (_type == "text_component" && defined(node))
]{_id, _type, name, path, coordinates, node, content, tag}`;

// This is a server component, so it's safe to use server-side fetching here.
export default async function Page() {
  const events = await sanityFetch<SanityDocument[]>({
    query: EVENTS_QUERY,
  });

  function LoadingScreen() {
    console.log('loading screen')
    return (
      <div style={{ textAlign: "center", marginTop: "50px" }}>
        <h1>Loading...</h1>
        {/* You can add a spinner or any other loading indicator here */}
      </div>
    );
  }

  return (
    <div>
      <Suspense fallback={<LoadingScreen />}>
        <MyComponent events={events}/>
      </Suspense>
    </div>
  );
}
